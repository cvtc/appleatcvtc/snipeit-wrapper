# SnipeIT Wrapper

[![GitLab](https://img.shields.io/gitlab/license/cvtc/appleatcvtc/carousel-api?style=flat-square)]()
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

SnipeIT Wrapper is a SnipeIT API wrapper that encapsulates tha endpoints that I need for projects.

## Table of Contents

- [SnipeIT Wrapper](#snipeit-wrapper)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Install](#install)
  - [Usage](#usage)
  - [Other Notes](#other-notes)
  - [Contributing](#contributing)
  - [License](#license)

## Background

This is a wrapper for the SnipeIT API with the goal of removing code duplication from SnipeIT projects. This wrapper will only gain functionality as I need it for projects.

## Install

To install SnipeIT API run the following:

```
pip install snipeit-wrapper --index-url https://gitlab.com/api/v4/projects/52621033/packages/pypi/simple
```

## Usage

The SnipeIT API only supports token auth so we just make a session defined as a variable and then use that for the API access that we need.

```
from os import environ
from snipeit_wrapper import SnipeIT

SNIPEIT_URL = environ["SNIPEIT_URL"]
SNIPEIT_TOKEN = environ["SNIPEIT_TOKEN"]

snipeit = SnipeIT(SNIPEIT_URL, SNIPEIT_TOKEN)

assets = snipeit.assets.get_by_search(search="iPad")
```

## Other Notes

- This API wrapper will most likely never be "finished" it's only purpose is to have a central place to make any future endpoints that I need for SnipeIT so I'm not duplicating code in projects

## Contributing

This repository contains a Pipfile for easy management of virtual environments
with Pipenv. Run it, but don't create a lockfile, to install the development
dependencies:

```
pipenv install --skip-lock --dev
```

To run the tests and get coverage information, run:

```
pipenv run pytest --cov=src --cov-report=xml --cov-report=term-missing
```

Files are formatted with Black prior to committing. Black is installed in your Pipenv virtual environment. Run it like this before you commit:

```
pipenv run black .
```

## License

[MIT © Bryan Weber.](./LICENSE)