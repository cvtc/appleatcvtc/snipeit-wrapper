import requests

from .endpoints import (
    Assets,
    Categories,
    Fields,
    Fieldsets,
    Locations,
    Manufacturers,
    Models,
    Users,
)
from .exceptions import SnipeITError


class SnipeIT:
    def __init__(self, snipeit_url, snipeit_token):
        """
        Creates an authenticated session with your SnipeIT instance and then
        handles other requests to the server

        :param snipeit_url: Base URL of your SnipeIT server
        :param snipeit_token: API token to authenticate to the server with
        """
        self.url = snipeit_url + "/api/v1"
        self.token = snipeit_token
        self.session = requests.session()
        self.session.headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.token}",
        }

        # endpoints
        self.assets = Assets(self)
        self.categories = Categories(self)
        self.fields = Fields(self)
        self.fieldsets = Fieldsets(self)
        self.locations = Locations(self)
        self.manufacturers = Manufacturers(self)
        self.models = Models(self)
        self.users = Users(self)

    @classmethod
    def _raise_recognized_errors(self, response: requests.Response):
        response_json = response.json()
        if "status" in response_json:
            if response_json["status"] == "error":
                raise SnipeITError(
                    "SnipeIT response contained an error:"
                    f"\n {response_json['messages']}"
                )

    def get(self, endpoint, params=None) -> dict:
        """
        General GET request method for Snipe-IT API.

        :param endpoint: The endpoint to which the GET request is made
        :param params: Optional parameters for the GET request
        :return: The response from the API
        """
        response = self.session.get(f"{self.url}{endpoint}", params=params)
        response.raise_for_status()
        self._raise_recognized_errors(response)
        return response.json()

    def post(self, endpoint, data, params=None) -> dict:
        """
        General POST request method for SnipeIT API.

        :params endpoint: The endpoint to which the POST request is made
        :param data: Data for request body of POST request
        :param params: Optional parameters for the POST request

        :returns: The response from the API
        """
        response = self.session.post(f"{self.url}{endpoint}", json=data, params=params)
        response.raise_for_status()
        self._raise_recognized_errors(response)
        return response.json()

    def put(self, endpoint, data, params=None) -> dict:
        """
        General PUT request method for SnipeIT API

        :params endpoint: The endpoint to which the PUT request is made
        :params data: Data for request body of PUT request
        :params params: Optional parameters for the PUT request

        :returns: The response from the API
        """
        response = self.session.put(f"{self.url}{endpoint}", json=data, params=params)
        response.raise_for_status()
        self._raise_recognized_errors(response)
        return response.json()

    def patch(self, endpoint, data, params=None) -> dict:
        """
        General PATCH request method for SnipeIT API

        :params endpoint: The endpoint to which the PATCH request is made
        :params data: Data for request body of PATCH request
        :params params: Optional parameters for the PATCH request

        :returns: The response from the API
        """
        response = self.session.patch(f"{self.url}{endpoint}", json=data, params=params)
        response.raise_for_status()
        self._raise_recognized_errors(response)
        return response.json()

    def delete(self, endpoint, data=None, params=None) -> dict:
        """
        General DELETE request method for SnipeIT API

        :params endpoint: The endpoint to which the DELETE request is made
        :params data: Data for request body of DELETE request
        :params params: Optional parameters for the DELETE request

        :returns: The response from the API
        """
        response = self.session.delete(
            f"{self.url}{endpoint}", json=data, params=params
        )
        response.raise_for_status()
        self._raise_recognized_errors(response)
        return response.json()
