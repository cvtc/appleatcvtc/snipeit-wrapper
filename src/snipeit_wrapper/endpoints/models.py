from typing import List

from ..models.ModelModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Models:
    """
    Models endpoint class
    """

    def __init__(self, api_client):
        self._api_client = api_client

    def get_by_search(
        self, search: str = None, sort: str = "created_at", order: str = "asc"
    ) -> List[dict]:
        """
        Get all models

        :param search: Search term
        :param sort:
            Column to sort by. Options are:
            id, name, manufacturer, image, model_number, min_amt, depreciation,
            assets_count, category, fieldset, eol, requestable, notes,
            created_at, updated_at, deleted_at, available_actions
        :param order: Sort order (asc or desc)

        :returns: List of returned models in ModelModel
        """
        params = get_params(locals())
        endpoint = "/models"
        SearchParameters(**params)
        models = iterate_through_pages(self._api_client.get, endpoint, params)

        return [model for model in models]

    def get_by_id(self, id: int) -> dict:
        """
        Get model by ID

        :param id: ID of the model

        :returns: Model data
        """
        endpoint = f"/models/{id}"

        return self._api_client.get(endpoint)

    def create(self, data: dict) -> dict:
        """
        Create model with model data

        :param data: Model data in dict

        :returns: New model data
        """
        endpoint = "/models"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update model with model data

        :param id: ID of the model
        :param data: Model data in dict

        :returns: Updated model data
        """
        endpoint = f"/models/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace model with model data

        :param id: ID of the model
        :param data: Model data in dict

        :returns: Replaced model data
        """
        endpoint = f"/models/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> dict:
        """
        Delete model by ID

        :param id: ID of the model

        :returns: Deleted model data
        """
        endpoint = f"/models/{id}"

        return self._api_client.delete(endpoint)["messages"]
