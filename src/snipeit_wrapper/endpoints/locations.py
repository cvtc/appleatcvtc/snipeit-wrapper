from typing import List

from ..models.LocationModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Locations:
    """
    Locations endpoint class
    """

    def __init__(self, api_client):
        self._api_client = api_client

    def get_by_search(
        self,
        search: str = None,
        sort: str = "created_at",
        order: str = "asc",
        address: str = None,
        address2: str = None,
        city: str = None,
        zip: str = None,
        country: str = None,
    ) -> List[dict]:
        """
        Get locations by search

        :param search: Search term
        :param sort:
            Column to sort by. Options are:
            id, name, image, address, address2, city, state, country, zip,
            phone, fax, assigned_assets_count, assets_count, rtd_assets_count,
            users_count, currency, ldap_ou, created_at, updated_at, parent,
            manager, children, available_actions
        :param order: Sort order. Options are: asc, desc
        :param address: Address
        :param address2: Address2
        :param city: City
        :param zip: Zip
        :param country: Country

        :returns: List of returned locations
        """
        params = get_params(locals())
        endpoint = "/locations"
        SearchParameters(**params)
        models = iterate_through_pages(self._api_client.get, endpoint, params)

        return [model for model in models]

    def get_by_id(self, id: int) -> dict:
        """
        Get location by ID

        :param id: ID of the location

        :returns: Location data
        """
        endpoint = f"/locations/{id}"

        return self._api_client.get(endpoint)

    def create(self, data: dict) -> dict:
        """
        Create location with location data

        :param data: Location data in dict

        :returns: New location data
        """
        endpoint = "/locations"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update location with location data

        :param id: ID of the location
        :param data: Location data in dict

        :returns: Updated location data
        """
        endpoint = f"/locations/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace location with location data

        :param id: ID of the location
        :param data: Location data in dict

        :returns: Replaced location data
        """
        endpoint = f"/locations/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> dict:
        """
        Delete location by ID

        :param id: ID of the location

        :returns: Deleted location data
        """
        endpoint = f"/locations/{id}"

        return self._api_client.delete(endpoint)["messages"]
