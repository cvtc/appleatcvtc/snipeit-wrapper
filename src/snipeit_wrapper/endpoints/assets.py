from typing import List

from ..models.AssetModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Assets:
    def __init__(self, api_client):
        self._api_client = api_client

    @classmethod
    def _image_check(self, asset: dict):
        """
        Checks if the image is set to a URL since it is returned that way
        in the GET requests but is expecting actual files for POST, PUT, and
        PATCH

        :param asset: Asset data in dict

        :returns: Asset dict with image field cleared if it is a URL
        """
        if "image" in asset and "https" in asset["image"]:
            del asset["image"]

        return asset

    def get_by_search(
        self,
        search: str = None,
        order_number: str = None,
        sort: str = None,
        order: str = None,
        model_id: int = None,
        category_id: int = None,
        manufacturer_id: int = None,
        company_id: int = None,
        location_id: int = None,
        status: str = None,
        status_id: int = None,
    ) -> List[dict]:
        """
        Search for assets, recommended to filter to reduce return size and time
        to return assets

        :param search: A text string to search the assets data for
        :param order_number:
            Return only assets associated with a specific order number
        :param sort:
            Specify the column name you wish to sort by
            Valid options are:
            id, name, asset_tag, serial, model, model_number, last_checkout,
            category, manufacturer, notes, expected_checkin, order_number,
            companyName, location, image, status_label, assigned_to,
            created_at, purchase_date, purchase_cost
        :param order:
            Specify the order (asc or desc) you wish to order by on your sort
            column
        :param model_id:
            Optionally restrict asset results to this asset model ID
        :param category_id:
            Optionally restrict asset results to this status label ID
        :param manufacturer_id:
            Optionally restrict asset results to this asset model ID
        :param company_id:
            Optionally restrict asset results to this company ID
        :param location_id:
            Optionally restrict asset results to this location ID
        :param status:
            Optionally restrict asset results to one of these status types:
            RTD, Deployed, Undeployable, Deleted, Archived, Requestable
        :param status_id:
            Optionally restrict asset results to this status label ID

        :returns: List of returned assets in AssetModel
        """
        params = get_params(locals())
        SearchParameters(**params)
        endpoint = "/hardware"
        assets = iterate_through_pages(self._api_client.get, endpoint, params)

        return [asset for asset in assets]

    def get_by_id(self, id: int) -> dict:
        """
        Get asset by ID

        :param id: ID of the asset

        :returns: Asset data
        """
        endpoint = f"/hardware/{id}"

        return self._api_client.get(endpoint)

    def get_by_asset_tag(self, asset_tag: str, deleted: bool = False) -> dict:
        """
        Get asset by asset tag

        :param asset_tag: Asset tag of the asset
        :param deleted: Include deleted items in your results

        :returns: Asset data
        """
        endpoint = f"/hardware/bytag/{asset_tag}"
        params = {"deleted": deleted}

        return self._api_client.get(endpoint, params=params)

    def get_by_serial(self, serial: str, deleted: bool = False) -> dict:
        """
        Get asset by serial

        :param serial: Serial of the asset
        :param deleted: Include deleted items in your results

        :returns: Asset data
        """
        endpoint = f"/hardware/byserial/{serial}"
        params = {"deleted": deleted}

        return self._api_client.get(endpoint, params=params)["rows"][0]

    def create(self, data: dict) -> dict:
        """
        Create asset with asset model

        :param data: Asset data in dict

        :returns: New asset data
        """
        endpoint = "/hardware"
        data = self._image_check(data)

        return self._api_client.post(endpoint, data=data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Updates asset with asset model

        :param data: Asset data in dict

        :returns: Updated asset data
        """
        data = self._image_check(data)
        endpoint = f"/hardware/{id}"

        return self._api_client.patch(endpoint, data=data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replaces asset data with asset model

        :param id: Asset ID
        :param data: Asset data in dict

        :returns: Replaced asset data
        """
        data = self._image_check(data)
        endpoint = f"/hardware/{id}"

        return self._api_client.put(endpoint, data=data)["payload"]

    def delete(self, id: int) -> str:
        """
        Deletes an asset with either the ID or a AssetModel

        :param id: Asset ID

        :returns: Message stating the asset was deleted
        """
        endpoint = f"/hardware/{id}"
        return self._api_client.delete(endpoint)["messages"]
