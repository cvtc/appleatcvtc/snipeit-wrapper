from typing import List

from ..models.UserModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Users:
    def __init__(self, api_client):
        self._api_client = api_client

    def get_by_search(
        self,
        search: str = None,
        sort: str = "created_at",
        order: str = "desc",
        first_name: str = None,
        last_name: str = None,
        username: str = None,
        email: str = None,
        employee_num: str = None,
        state: str = None,
        zip: str = None,
        country: str = None,
        group_id: int = None,
        department_id: int = None,
        company_id: int = None,
        location_id: int = None,
        deleted: bool = False,
        all: bool = False,
        ldap_import: bool = False,
        assets_count: int = None,
        licenses_count: int = None,
        accessories_count: int = None,
        consumables_count: int = None,
        remote: bool = None,
        vip: int = None,
        start_date: str = None,
        end_date: str = None,
    ) -> List[dict]:
        """
        Returns all users based on search or other criteria

        :param search: String to search on
        :param sort: Field to order by
        :param order: Sort order (asc or desc)
        :param first_name: First name of the User
        :param last_name: Last name of the user
        :param username: Username of the user
        :param email: Email of the user
        :param employee_num: Employee number of the user
        :param state: State of the user
        :param zip: Zip code of the user
        :param country: Country of the user
        :param group_id: Group ID of the user
        :param department_id: Department ID of the user
        :param company_id: Company ID of the user
        :param location_id: Location ID of the user
        :param deleted:
            Set this to "true" if you want to return only deleted users
        :param all:
            Set this to "true" if you want both deleted and active users
        :param ldap_import:
            Whether the user was imported/synched with LDAP
        :param assets_count: Number of checked out assets
        :param licenses_count: Number of checked out licenses
        :param accessories_count: Number of checked out accessories
        :param consumables_count: Number of checked out consumables
        :param remote: Whether the user is marked as a remote worker or not
        :param vip:
            Whether or not the user is marked as a VIP (1 or 0 for true or
            false, respectively)
        :param start_date: Start date of the user
        :param end_date: End date of the user

        :returns: List of returned users
        """
        params = get_params(locals())
        SearchParameters(**params)
        endpoint = "/users"
        users = iterate_through_pages(self._api_client.get, endpoint, params)

        return [user for user in users]

    def get_by_id(self, id: int) -> dict:
        """
        Get user by ID

        :param id: ID of the user

        :returns: User data
        """
        endpoint = f"/users/{id}"

        return self._api_client.get(endpoint)

    def create(self, data: dict) -> dict:
        """
        Create user with user data

        :param data: User data in dict

        :returns: New user data
        """
        endpoint = "/users"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update user with user data

        :param id: ID of the user
        :param data: User data in dict

        :returns: Updated user data
        """
        endpoint = f"/users/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace user with user data

        :param id: ID of the user
        :param data: User data in dict

        :returns: Replaced user data
        """
        endpoint = f"/users/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> dict:
        """
        Delete user by ID

        :param id: ID of the user

        :returns: Deleted user data
        """
        endpoint = f"/users/{id}"

        return self._api_client.delete(endpoint)["messages"]
