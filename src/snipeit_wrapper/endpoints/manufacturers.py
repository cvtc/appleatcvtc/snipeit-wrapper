from typing import List

from ..models.ManufacturerModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Manufacturers:
    def __init__(self, api_client):
        self._api_client = api_client

    def get_by_search(
        self,
        name: str = None,
        url: str = None,
        support_url: str = None,
        support_phone: str = None,
        support_email: str = None,
    ) -> List[dict]:
        """
        Get manufacturers by search

        :param name: Name of the manufacturer
        :param url: URL of the manufacturer
        :param support_url: Support URL of the manufacturer
        :param support_phone: Support phone of the manufacturer
        :param support_email: Support email of the manufacturer

        :returns: List of returned manufacturers in dicts
        """
        params = get_params(locals())
        SearchParameters(**params)
        endpoint = "/manufacturers"
        manufacturers = iterate_through_pages(self._api_client.get, endpoint, params)

        return [manufacturer for manufacturer in manufacturers]

    def get_by_id(self, id: int) -> dict:
        """
        Get manufacturer by ID

        :param id: ID of the manufacturer

        :returns: Manufacturer data
        """
        endpoint = f"/manufacturers/{id}"

        return self._api_client.get(endpoint)

    def create(self, data: dict) -> dict:
        """
        Create manufacturer with manufacturer data

        :param data: Manufacturer data in dict

        :returns: New manufacturer data
        """
        endpoint = "/manufacturers"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update manufacturer with manufacturer data

        :param id: ID of the manufacturer
        :param data: Manufacturer data in dict

        :returns: Updated manufacturer data
        """
        endpoint = f"/manufacturers/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace manufacturer with manufacturer data

        :param id: ID of the manufacturer
        :param data: Manufacturer data in dict

        :returns: Replaced manufacturer data
        """
        endpoint = f"/manufacturers/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> str:
        """
        Delete manufacturer by ID

        :param id: ID of the manufacturer

        :returns: Success message
        """
        endpoint = f"/manufacturers/{id}"

        return self._api_client.delete(endpoint)["messages"]
