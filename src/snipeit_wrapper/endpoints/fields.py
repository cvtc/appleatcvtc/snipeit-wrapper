from typing import List


class Fields:
    def __init__(self, api_client):
        self._api_client = api_client

    def get_all(self) -> List[dict]:
        """
        Get all fields

        :returns: List of returned fields in dicts
        """
        endpoint = "/fields"

        return self._api_client.get(endpoint)["rows"]

    def get_by_id(self, id: int) -> dict:
        """
        Get field by ID

        :param id: ID of the field

        :returns: Field data in dict
        """
        endpoint = f"/fields/{id}"

        return self._api_client.get(endpoint)

    def associate_with_fieldset(self, id: int, fieldset_id: int) -> str:
        """
        Associate field with fieldset

        :param id: ID of the field
        :param fieldset_id: ID of the fieldset

        :returns: Field data in dict
        """
        endpoint = f"/fields/{id}/associate"
        data = {"fieldset_id": fieldset_id}

        return self._api_client.post(endpoint, data)["messages"]

    def dissociate_with_fieldset(self, id: int, fieldset_id: int) -> dict:
        """
        Dissociate field with fieldset

        :param id: ID of the field
        :param fieldset_id: ID of the fieldset

        :returns: Field data in dict
        """
        endpoint = f"/fields/{id}/dissociate"
        data = {"fieldset_id": fieldset_id}

        return self._api_client.post(endpoint, data)["messages"]

    def create(self, data: dict) -> dict:
        """
        Create field with field data

        :param data:
            Field data in dict, for full syntax information see
            https://snipe-it.readme.io/reference/fields-2

        :returns: New field data in dict
        """
        endpoint = "/fields"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update field with field data

        :param id: ID of the field
        :param data:
            Field data in dict, for full syntax information see
            https://snipe-it.readme.io/reference/fieldsid-2

        :returns: Updated field data in dict
        """
        endpoint = f"/fields/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace field with field data

        :param id: ID of the field
        :param data:
            Field data in dict, for full syntax information see
            https://snipe-it.readme.io/reference/update-fields

        :returns: Replaced field data in dict
        """
        endpoint = f"/fields/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> str:
        """
        Delete field by ID

        :param id: ID of the field

        :returns: Success message
        """
        endpoint = f"/fields/{id}"

        return self._api_client.delete(endpoint)["messages"]
