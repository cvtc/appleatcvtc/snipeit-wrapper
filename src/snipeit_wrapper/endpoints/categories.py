from typing import List

from ..models.CategoryModels import SearchParameters
from ..utils import get_params, iterate_through_pages


class Categories:
    def __init__(self, api_client):
        self._api_client = api_client

    def get_by_search(
        self,
        name: str = None,
        search: str = None,
        sort: str = "created_at",
        order: str = "asc",
        category_id: int = None,
        category_type: str = None,
        use_default_eula: bool = None,
        require_acceptance: bool = None,
        checkin_email: bool = None,
    ) -> List[dict]:
        """
        Search for categories, recommended to use name or search parameter
        to reduce size of return

        :param name: Name of the category
        :param search: Search term
        :param sort:
            Column to sort by. Options are:
            id, name, category_type, use_default_eula, eula, checkin_email,
            require_acceptance, item_count, assets_count, accessories_count,
            consumables_count, components_count, licenses_count, created_at,
            updated_at, available_actions
        :param order: Sort order (asc or desc)
        :param category_id: ID of the category
        :param category_type: Type of the category
        :param use_default_eula: Whether or not category uses default EULA
        :param require_acceptance: Whether or not acceptance is required
        :param checkin_email: Checkin email

        :returns: List of returned categories in dicts
        """
        params = get_params(locals())
        SearchParameters(**params)
        endpoint = "/categories"
        categories = iterate_through_pages(self._api_client.get, endpoint, params)

        return [category for category in categories]

    def get_by_id(self, id: int) -> dict:
        """
        Get category by ID

        :param id: ID of the category

        :returns: Category data
        """
        endpoint = f"/categories/{id}"

        return self._api_client.get(endpoint)

    def create(self, data: dict) -> dict:
        """
        Create category with category data

        :param data: Category data in dict

        :returns: New category data
        """
        endpoint = "/categories"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update category with category data

        :param id: ID of the category
        :param data: Category data in dict

        :returns: Updated category data
        """
        endpoint = f"/categories/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace category with category data

        :param id: ID of the category
        :param data: Category data in dict

        :returns: Replaced category data
        """
        endpoint = f"/categories/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> dict:
        """
        Delete category by ID

        :param id: ID of the category

        :returns: Deletion status
        """
        endpoint = f"/categories/{id}"

        return self._api_client.delete(endpoint)
