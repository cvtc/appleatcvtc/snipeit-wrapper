from typing import List, Union


class Fieldsets:
    def __init__(self, api_client):
        self._api_client = api_client

    def get_all(self) -> List[dict]:
        """
        Get all fieldsets

        :returns: List of returned fieldsets in dicts
        """
        endpoint = "/fieldsets"

        return self._api_client.get(endpoint)["rows"]

    def get_by_id(self, id: int) -> dict:
        """
        Get fieldset by ID

        :param id: ID of the fieldset

        :returns: Fieldset data in dict
        """
        endpoint = f"/fieldsets/{id}"

        return self._api_client.get(endpoint)

    def get_fields_by_id(self, id: int) -> List[dict]:
        """
        Get fields of fieldset ID

        :param id: ID of the fieldset

        :returns: List of fields in dicts
        """
        endpoint = f"/fieldsets/{id}/fields"

        return self._api_client.get(endpoint)["rows"]

    def create(self, data: dict) -> dict:
        """
        Create fieldset with fieldset data

        :param data: Fieldset data in dict

        :returns: New fieldset data
        """
        endpoint = "/fieldsets"

        return self._api_client.post(endpoint, data)["payload"]

    def update(self, id: int, data: dict) -> dict:
        """
        Update fieldset with fieldset data

        :param id: ID of the fieldset
        :param data: Fieldset data in dict

        :returns: Updated fieldset data
        """
        endpoint = f"/fieldsets/{id}"

        return self._api_client.patch(endpoint, data)["payload"]

    def replace(self, id: int, data: dict) -> dict:
        """
        Replace fieldset with fieldset data

        :param id: ID of the fieldset
        :param data: Fieldset data in dict

        :returns: Replaced fieldset data
        """
        endpoint = f"/fieldsets/{id}"

        return self._api_client.put(endpoint, data)["payload"]

    def delete(self, id: int) -> str:
        """
        Delete fieldset by ID

        :param id: ID of the fieldset

        :returns: Deleted fieldset data
        """
        endpoint = f"/fieldsets/{id}"

        return self._api_client.delete(endpoint)["messages"]
