class SnipeITError(Exception):
    """
    Raise when SnipeIT returns a 200 response but is actually an error
    """


class MissingID(Exception):
    """
    Raise when the id field is required for a method but is not present. This
    is a separate error because it cannot be made required in the model since
    the POST requests cannot have the ID present.
    """
