from typing import Optional

from pydantic import BaseModel


class SearchParameters(BaseModel):
    name: Optional[str] = None
    url: Optional[str] = None
    support_url: Optional[str] = None
    support_phone: Optional[str] = None
    support_email: Optional[str] = None
    limit: Optional[int] = None
    offset: Optional[int] = None
