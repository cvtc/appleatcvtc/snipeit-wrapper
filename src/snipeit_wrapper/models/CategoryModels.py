from enum import Enum
from typing import Optional

from pydantic import BaseModel, ConfigDict

from .GeneralModels import Order


class Sort(str, Enum):
    ID = "id"
    NAME = "name"
    CATEGORY_TYPE = "category_type"
    HAS_EULA = "has_eula"
    USE_DEFAULT_EULA = "use_default_eula"
    EULA = "eula"
    CHECKIN_EMAIL = "checkin_email"
    REQUIRE_ACCEPTANCE = "require_acceptance"
    ITEM_COUNT = "item_count"
    ASSETS_COUNT = "assets_count"
    ACCESSORIES_COUNT = "accessories_count"
    CONSUMABLES_COUNT = "consumables_count"
    COMPONENTS_COUNT = "components_count"
    LICENSES_COUNT = "licenses_count"
    CREATED_AT = "created_at"
    UPDATED_AT = "updated_at"
    AVAILABLE_ACTIONS = "available_actions"


class SearchParameters(BaseModel):
    model_config = ConfigDict()

    name: Optional[str] = None
    search: Optional[str] = None
    sort: Optional[Sort] = "created_at"
    order: Optional[Order] = "asc"
    category_id: Optional[int] = None
    category_type: Optional[str] = None
    use_default_eula: Optional[bool] = None
    require_acceptance: Optional[bool] = None
    checkin_email: Optional[bool] = None
