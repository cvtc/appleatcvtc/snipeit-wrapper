from enum import Enum
from typing import Optional

from pydantic import BaseModel, ConfigDict

from .GeneralModels import Order


class Sort(str, Enum):
    ID = "id"
    NAME = "name"
    ASSET_TAG = "asset_tag"
    SERIAL = "serial"
    MODEL = "model"
    MODEL_NUMBER = "model_number"
    LAST_CHECKOUT = "last_checkout"
    CATEGORY = "category"
    MANUFACTURER = "manufacturer"
    NOTES = "notes"
    EXPECTED_CHECKIN = "expected_checkin"
    ORDER_NUMBER = "order_number"
    COMPANY_NAME = "companyName"
    LOCATION = "location"
    IMAGE = "image"
    STATUS_LABEL = "status_label"
    ASSIGNED_TO = "assigned_to"
    CREATED_AT = "created_at"
    PURCHASE_DATE = "purchase_date"
    PURCHASE_COST = "purchase_cost"


class Status(str, Enum):
    RTD = "RTD"
    DEPLOYED = "Deployed"
    UNDEPLOYABLE = "Undeployable"
    DELETED = "Deleted"
    ARCHIVED = "Archived"
    REQUESTABLE = "Requestable"


class SearchParameters(BaseModel):
    model_config = ConfigDict(protected_namespaces=())

    limit: Optional[int] = None
    offset: Optional[int] = None
    search: Optional[str] = None
    order_number: Optional[str] = None
    sort: Optional[Sort] = "created_at"
    order: Optional[Order] = "desc"
    model_id: Optional[int] = None
    category_id: Optional[int] = None
    manufacturer_id: Optional[int] = None
    company_id: Optional[int] = None
    status: Optional[Status] = None
    status_id: Optional[int] = None
