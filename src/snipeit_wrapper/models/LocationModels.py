from enum import Enum
from typing import Optional

from pydantic import BaseModel, ConfigDict

from .GeneralModels import Order


class Sort(str, Enum):
    ID = "id"
    NAME = "name"
    IMAGE = "image"
    ADDRESS = "address"
    ADDRESS2 = "address2"
    CITY = "city"
    STATE = "state"
    COUNTRY = "country"
    ZIP = "zip"
    PHONE = "phone"
    FAX = "fax"
    ASSIGNED_ASSETS_COUNT = "assigned_assets_count"
    ASSETS_COUNT = "assets_count"
    RTD_ASSETS_COUNT = "rtd_assets_count"
    USERS_COUNT = "users_count"
    CURRENCY = "currency"
    LDAP_OU = "ldap_ou"
    CREATED_AT = "created_at"
    UPDATED_AT = "updated_at"
    PARENT = "parent"
    MANAGER = "manager"
    CHILDREN = "children"
    AVAILABLE_ACTIONS = "available_actions"


class SearchParameters(BaseModel):
    model_config = ConfigDict(protected_namespaces=())

    limit: Optional[int] = None
    offset: Optional[int] = None
    search: Optional[str] = None
    sort: Optional[Sort] = "created_at"
    order: Optional[Order] = "asc"
