from enum import Enum

from pydantic import BaseModel

from .GeneralModels import Order


class Sort(str, Enum):
    ID = "id"
    AVATAR = "avatar"
    NAME = "name"
    FIRST_NAME = "first_name"
    LAST_NAME = "last_name"
    USERNAME = "username"
    REMOTE = "remote"
    LOCALE = "locale"
    EMPLOYEE_NUM = "employee_num"
    MANAGER = "manager"
    JOBTITLE = "jobtitle"
    VIP = "vip"
    PHONE = "phone"
    WEBSITE = "website"
    ADDRESS = "address"
    CITY = "city"
    STATE = "state"
    COUNTRY = "country"
    ZIP = "zip"
    EMAIL = "email"
    DEPARTMENT = "department"
    LOCATION = "location"
    NOTES = "notes"
    PERMISSIONS = "permissions"
    ACTIVATED = "activated"
    AUTOASSIGN_LICENSES = "autoassign_licenses"
    LDAP_IMPORT = "ldap_import"
    TWO_FACTOR_ENROLLED = "two_factor_enrolled"
    TWO_FACTOR_OPTIN = "two_factor_optin"
    ASSETS_COUNT = "assets_count"
    LICENSES_COUNT = "licenses_count"
    ACCESSORIES_COUNT = "accessories_count"
    CONSUMABLES_COUNT = "consumables_count"
    COMPANY = "company"
    CREATED_BY = "created_by"
    CREATED_AT = "created_at"
    UPDATED_AT = "updated_at"
    START_DATE = "start_date"
    END_DATE = "end_date"
    LAST_LOGIN = "last_login"
    DELETED_AT = "deleted_at"
    AVAILABLE_ACTIONS = "available_actions"
    GROUPS = "groups"


class SearchParameters(BaseModel):
    search: str = None
    sort: str = "created_at"
    order: Order = "desc"
    first_name: str = None
    last_name: str = None
    username: str = None
    email: str = None
    employee_num: str = None
    state: str = None
    zip: str = None
    country: str = None
    group_id: int = None
    department_id: int = None
    company_id: int = None
    location_id: int = None
    deleted: bool = False
    all: bool = False
    ldap_import: bool = False
    assets_count: int = None
    licenses_count: int = None
    accessories_count: int = None
    consumables_count: int = None
    remote: bool = None
    vip: int = None
    start_date: str = None
    end_date: str = None
