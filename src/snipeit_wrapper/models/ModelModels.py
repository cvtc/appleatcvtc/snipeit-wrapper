from enum import Enum
from typing import Optional

from pydantic import BaseModel, ConfigDict

from .GeneralModels import Order


class Sort(str, Enum):
    ID = "id"
    NAME = "name"
    MANUFACTURER = "manufacturer"
    IMAGE = "image"
    MODEL_NUMBER = "model_number"
    MIN_AMT = "min_amt"
    DEPRECIATION = "depreciation"
    ASSETS_COUNT = "assets_count"
    CATEGORY = "category"
    FIELDSET = "fieldset"
    EOL = "eol"
    REQUESTABLE = "requestable"
    NOTES = "notes"
    CREATED_AT = "created_at"
    UPDATED_AT = "updated_at"
    DELETED_AT = "deleted_at"
    AVAILABLE_ACTIONS = "available_actions"


class SearchParameters(BaseModel):
    model_config = ConfigDict(protected_namespaces=())

    limit: Optional[int] = None
    offset: Optional[int] = None
    search: Optional[str] = None
    sort: Optional[Sort] = "created_at"
    order: Optional[Order] = "asc"
