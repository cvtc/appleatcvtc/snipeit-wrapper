from typing import List


def get_params(arguments: dict, ignore_arguments: List[str] = None) -> dict:
    """
        Returns a dictionary of parameters from a list of locals

        :param arguments: Use locals() from within a function
        :param ignore_arguments:
            Method arguments you want ignored, self is ignored by default
    y
        :returns: Dict of parameter values
    """
    if ignore_arguments:
        ignore_arguments = ["self"].extend(ignore_arguments)
    else:
        ignore_arguments = ["self"]
    return {k: v for k, v in arguments.items() if k not in ignore_arguments and v}


def iterate_through_pages(
    api_method, endpoint: str, initial_params: dict = None
) -> dict:
    """
    Iterate through all pages of a returned GET request

    :param api_method:
        Typically a SnipeIT.get method, but any SnipeIT client method works
    :param endpoints: API endpoint
    :param initial_params: Dictionary of URL params

    :returns: Dictionary of all pages of a get request
    """
    offset = 0
    limit = initial_params.get("limit", 100)  # Set a default limit if not provided

    while True:
        params = initial_params.copy()
        params["limit"] = limit
        params["offset"] = offset
        response_data = api_method(endpoint, params=params)

        # Extract the data rows from the response
        rows = response_data.get("rows", [])
        for row in rows:
            yield row

        # Check if we have reached the end
        if len(rows) < limit:
            break

        # Increment the offset
        offset += limit

    return
