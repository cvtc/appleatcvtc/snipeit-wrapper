from snipeit_wrapper.models.UserModels import SearchParameters, Sort


def test_sort_enum():
    assert Sort.ID == "id"
    assert Sort.AVATAR == "avatar"
    assert Sort.NAME == "name"
    assert Sort.FIRST_NAME == "first_name"
    assert Sort.LAST_NAME == "last_name"
    assert Sort.USERNAME == "username"
    assert Sort.REMOTE == "remote"
    assert Sort.LOCALE == "locale"
    assert Sort.EMPLOYEE_NUM == "employee_num"
    assert Sort.MANAGER == "manager"
    assert Sort.JOBTITLE == "jobtitle"
    assert Sort.VIP == "vip"
    assert Sort.PHONE == "phone"
    assert Sort.WEBSITE == "website"
    assert Sort.ADDRESS == "address"
    assert Sort.CITY == "city"
    assert Sort.STATE == "state"
    assert Sort.COUNTRY == "country"
    assert Sort.ZIP == "zip"
    assert Sort.EMAIL == "email"
    assert Sort.DEPARTMENT == "department"
    assert Sort.LOCATION == "location"
    assert Sort.NOTES == "notes"
    assert Sort.PERMISSIONS == "permissions"
    assert Sort.ACTIVATED == "activated"
    assert Sort.AUTOASSIGN_LICENSES == "autoassign_licenses"
    assert Sort.LDAP_IMPORT == "ldap_import"
    assert Sort.TWO_FACTOR_ENROLLED == "two_factor_enrolled"
    assert Sort.TWO_FACTOR_OPTIN == "two_factor_optin"
    assert Sort.ASSETS_COUNT == "assets_count"
    assert Sort.LICENSES_COUNT == "licenses_count"
    assert Sort.ACCESSORIES_COUNT == "accessories_count"
    assert Sort.CONSUMABLES_COUNT == "consumables_count"
    assert Sort.COMPANY == "company"
    assert Sort.CREATED_BY == "created_by"
    assert Sort.CREATED_AT == "created_at"
    assert Sort.UPDATED_AT == "updated_at"
    assert Sort.START_DATE == "start_date"
    assert Sort.END_DATE == "end_date"
    assert Sort.LAST_LOGIN == "last_login"
    assert Sort.DELETED_AT == "deleted_at"
    assert Sort.AVAILABLE_ACTIONS == "available_actions"
    assert Sort.GROUPS == "groups"


def test_search_parameters_default_values():
    search_params = SearchParameters()
    assert search_params.search is None
    assert search_params.sort == "created_at"
    assert search_params.order == "desc"
    assert search_params.first_name is None
    assert search_params.last_name is None
    assert search_params.username is None
    assert search_params.email is None
    assert search_params.employee_num is None
    assert search_params.state is None
    assert search_params.zip is None
    assert search_params.country is None
    assert search_params.group_id is None
    assert search_params.department_id is None
    assert search_params.company_id is None
    assert search_params.location_id is None
    assert search_params.deleted is False
    assert search_params.all is False
    assert search_params.ldap_import is False
    assert search_params.assets_count is None
    assert search_params.licenses_count is None
    assert search_params.accessories_count is None
    assert search_params.consumables_count is None
    assert search_params.remote is None
    assert search_params.vip is None
    assert search_params.start_date is None
    assert search_params.end_date is None


def test_search_parameters_example_data():
    search_params = SearchParameters(
        search="John",
        sort="name",
        order="asc",
        first_name="John",
        last_name="Doe",
        username="johndoe",
        email="johndoe@example.com",
        employee_num="12345",
        state="CA",
        zip="12345",
        country="USA",
        group_id=1,
        department_id=2,
        company_id=3,
        location_id=4,
        deleted=True,
        all=True,
        ldap_import=True,
        assets_count=10,
        licenses_count=5,
        accessories_count=3,
        consumables_count=2,
        remote=True,
        vip=1,
        start_date="2022-01-01",
        end_date="2022-12-31",
    )
    assert search_params.search == "John"
    assert search_params.sort == "name"
    assert search_params.order == "asc"
    assert search_params.first_name == "John"
    assert search_params.last_name == "Doe"
    assert search_params.username == "johndoe"
    assert search_params.email == "johndoe@example.com"
    assert search_params.employee_num == "12345"
    assert search_params.state == "CA"
    assert search_params.zip == "12345"
    assert search_params.country == "USA"
    assert search_params.group_id == 1
    assert search_params.department_id == 2
    assert search_params.company_id == 3
    assert search_params.location_id == 4
    assert search_params.deleted is True
    assert search_params.all is True
    assert search_params.ldap_import is True
    assert search_params.assets_count == 10
    assert search_params.licenses_count == 5
    assert search_params.accessories_count == 3
    assert search_params.consumables_count == 2
    assert search_params.remote is True
    assert search_params.vip == 1
    assert search_params.start_date == "2022-01-01"
    assert search_params.end_date == "2022-12-31"
