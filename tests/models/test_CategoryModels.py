from snipeit_wrapper.models.CategoryModels import Sort, SearchParameters


def test_sort_enum_values():
    assert Sort.ID == "id"
    assert Sort.NAME == "name"
    assert Sort.CATEGORY_TYPE == "category_type"
    assert Sort.HAS_EULA == "has_eula"
    assert Sort.USE_DEFAULT_EULA == "use_default_eula"
    assert Sort.EULA == "eula"
    assert Sort.CHECKIN_EMAIL == "checkin_email"
    assert Sort.REQUIRE_ACCEPTANCE == "require_acceptance"
    assert Sort.ITEM_COUNT == "item_count"
    assert Sort.ASSETS_COUNT == "assets_count"
    assert Sort.ACCESSORIES_COUNT == "accessories_count"
    assert Sort.CONSUMABLES_COUNT == "consumables_count"
    assert Sort.COMPONENTS_COUNT == "components_count"
    assert Sort.LICENSES_COUNT == "licenses_count"
    assert Sort.CREATED_AT == "created_at"
    assert Sort.UPDATED_AT == "updated_at"
    assert Sort.AVAILABLE_ACTIONS == "available_actions"


def test_search_params():
    params = SearchParameters()
    assert params.name is None
    assert params.search is None
    assert params.sort == "created_at"
    assert params.order == "asc"
    assert params.category_id is None
    assert params.category_type is None
    assert params.use_default_eula is None
    assert params.require_acceptance is None
    assert params.checkin_email is None
