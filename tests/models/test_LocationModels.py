from snipeit_wrapper.models.LocationModels import Sort, SearchParameters


def test_sort_enum_values():
    assert Sort.ID == "id"
    assert Sort.NAME == "name"
    assert Sort.IMAGE == "image"
    assert Sort.ADDRESS == "address"
    assert Sort.ADDRESS2 == "address2"
    assert Sort.CITY == "city"
    assert Sort.STATE == "state"
    assert Sort.COUNTRY == "country"
    assert Sort.ZIP == "zip"
    assert Sort.PHONE == "phone"
    assert Sort.FAX == "fax"
    assert Sort.ASSIGNED_ASSETS_COUNT == "assigned_assets_count"
    assert Sort.ASSETS_COUNT == "assets_count"
    assert Sort.RTD_ASSETS_COUNT == "rtd_assets_count"
    assert Sort.USERS_COUNT == "users_count"
    assert Sort.CURRENCY == "currency"
    assert Sort.LDAP_OU == "ldap_ou"
    assert Sort.CREATED_AT == "created_at"
    assert Sort.UPDATED_AT == "updated_at"
    assert Sort.PARENT == "parent"
    assert Sort.MANAGER == "manager"
    assert Sort.CHILDREN == "children"
    assert Sort.AVAILABLE_ACTIONS == "available_actions"


def test_search_parameters_default_values():
    search_params = SearchParameters()
    assert search_params.limit is None
    assert search_params.offset is None
    assert search_params.search is None
    assert search_params.sort == Sort.CREATED_AT
    assert search_params.order == "asc"


def test_search_parameters_custom_values():
    search_params = SearchParameters(
        limit=10, offset=20, search="example", sort=Sort.NAME, order="desc"
    )
    assert search_params.limit == 10
    assert search_params.offset == 20
    assert search_params.search == "example"
    assert search_params.sort == Sort.NAME
    assert search_params.order == "desc"
