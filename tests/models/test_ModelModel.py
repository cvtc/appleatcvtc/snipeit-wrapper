from snipeit_wrapper.models.GeneralModels import Order
from snipeit_wrapper.models.ModelModels import SearchParameters, Sort


def test_sort_enum():
    assert Sort.ID == "id"
    assert Sort.NAME == "name"
    assert Sort.MANUFACTURER == "manufacturer"
    assert Sort.IMAGE == "image"
    assert Sort.MODEL_NUMBER == "model_number"
    assert Sort.MIN_AMT == "min_amt"
    assert Sort.DEPRECIATION == "depreciation"
    assert Sort.ASSETS_COUNT == "assets_count"
    assert Sort.CATEGORY == "category"
    assert Sort.FIELDSET == "fieldset"
    assert Sort.EOL == "eol"
    assert Sort.REQUESTABLE == "requestable"
    assert Sort.NOTES == "notes"
    assert Sort.CREATED_AT == "created_at"
    assert Sort.UPDATED_AT == "updated_at"
    assert Sort.DELETED_AT == "deleted_at"
    assert Sort.AVAILABLE_ACTIONS == "available_actions"


def test_search_parameters_default_values():
    search_params = SearchParameters()
    assert search_params.limit is None
    assert search_params.offset is None
    assert search_params.search is None
    assert search_params.sort == Sort.CREATED_AT
    assert search_params.order == Order.ASC
