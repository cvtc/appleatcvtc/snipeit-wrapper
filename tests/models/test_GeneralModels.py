from snipeit_wrapper.models.GeneralModels import Order


def test_order_enum():
    assert Order.ASC == "asc"
    assert Order.DESC == "desc"
