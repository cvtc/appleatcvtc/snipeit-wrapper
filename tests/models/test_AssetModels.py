from snipeit_wrapper.models.AssetModels import Order, SearchParameters, Sort, Status


def test_sort_enum():
    assert Sort.ID == "id"
    assert Sort.NAME == "name"
    assert Sort.ASSET_TAG == "asset_tag"
    assert Sort.SERIAL == "serial"
    assert Sort.MODEL == "model"
    assert Sort.MODEL_NUMBER == "model_number"
    assert Sort.LAST_CHECKOUT == "last_checkout"
    assert Sort.CATEGORY == "category"
    assert Sort.MANUFACTURER == "manufacturer"
    assert Sort.NOTES == "notes"
    assert Sort.EXPECTED_CHECKIN == "expected_checkin"
    assert Sort.ORDER_NUMBER == "order_number"
    assert Sort.COMPANY_NAME == "companyName"
    assert Sort.LOCATION == "location"
    assert Sort.IMAGE == "image"
    assert Sort.STATUS_LABEL == "status_label"
    assert Sort.ASSIGNED_TO == "assigned_to"
    assert Sort.CREATED_AT == "created_at"
    assert Sort.PURCHASE_DATE == "purchase_date"
    assert Sort.PURCHASE_COST == "purchase_cost"


def test_order_enum():
    assert Order.ASC == "asc"
    assert Order.DESC == "desc"


def test_status_enum():
    assert Status.RTD == "RTD"
    assert Status.DEPLOYED == "Deployed"


def test_search_parameters_defaults():
    params = SearchParameters()
    assert params.limit is None
    assert params.offset is None
    assert params.search is None
    assert params.order_number is None
    assert params.sort == "created_at"
    assert params.order == "desc"
    assert params.model_id is None
    assert params.category_id is None
    assert params.manufacturer_id is None
    assert params.company_id is None
    assert params.status is None
    assert params.status_id is None


def test_search_parameters_assignment():
    params = SearchParameters(
        limit=10,
        offset=5,
        search="test",
        order_number="1234",
        sort=Sort.ID,
        order=Order.ASC,
        model_id=1,
        category_id=2,
        manufacturer_id=3,
        company_id=4,
        status=Status.RTD,
        status_id=6,
    )
    assert params.limit == 10
    assert params.offset == 5
    assert params.search == "test"
    assert params.order_number == "1234"
    assert params.sort == Sort.ID
    assert params.order == Order.ASC
    assert params.model_id == 1
    assert params.category_id == 2
    assert params.manufacturer_id == 3
    assert params.company_id == 4
    assert params.status == Status.RTD
    assert params.status_id == 6
