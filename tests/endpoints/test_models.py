import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints import Models


@responses.activate
def test_get_by_search(snipeit):
    models = Models(snipeit)
    search_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
    }
    expected_url = snipeit_url("/models")
    expected_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "limit": "100",
        "offset": "0",
    }
    responses.add(
        responses.GET, expected_url, json=ExpectedResponse.list_send, status=200
    )
    response = models.get_by_search(**search_params)
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    models = Models(snipeit)
    expected_url = snipeit_url("/models/1")
    responses.add(
        responses.GET, expected_url, json=ExpectedResponse.default, status=200
    )
    response = models.get_by_id(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_create(snipeit):
    models = Models(snipeit)
    expected_url = snipeit_url("/models")
    responses.add(
        responses.POST, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = models.create({})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    models = Models(snipeit)
    expected_url = snipeit_url("/models/1")
    responses.add(
        responses.PATCH, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = models.update(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    models = Models(snipeit)
    expected_url = snipeit_url("/models/1")
    responses.add(
        responses.PUT, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = models.replace(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    models = Models(snipeit)
    expected_url = snipeit_url("/models/1")
    responses.add(
        responses.DELETE, expected_url, json=ExpectedResponse.messages_send, status=200
    )
    response = models.delete(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return
