import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints.categories import Categories


@responses.activate
def test_get_by_search(snipeit):
    categories = Categories(snipeit)
    search_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "category_id": 2,
        "category_type": "asset",
        "use_default_eula": True,
        "require_acceptance": True,
        "checkin_email": True,
    }
    expected_url = snipeit_url("/categories")
    expected_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "category_id": "2",
        "category_type": "asset",
        "use_default_eula": "True",
        "require_acceptance": "True",
        "checkin_email": "True",
        "limit": "100",
        "offset": "0",
    }

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
        status=200,
    )

    response = categories.get_by_search(**search_params)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    categories = Categories(snipeit)
    category_id = 123
    expected_url = snipeit_url(f"/categories/{category_id}")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = categories.get_by_id(category_id)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.default


@responses.activate
def test_create(snipeit):
    categories = Categories(snipeit)
    data = {"name": "Test Category"}
    expected_url = snipeit_url("/categories")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = categories.create(data)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    categories = Categories(snipeit)
    category_id = 123
    data = {"name": "Test Category"}
    expected_url = snipeit_url(f"/categories/{category_id}")

    responses.add(
        responses.PATCH,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = categories.update(category_id, data)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    categories = Categories(snipeit)
    category_id = 123
    data = {"name": "Test Category"}
    expected_url = snipeit_url(f"/categories/{category_id}")

    responses.add(
        responses.PUT,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = categories.replace(category_id, data)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    categories = Categories(snipeit)
    category_id = 123
    expected_url = snipeit_url(f"/categories/{category_id}")

    responses.add(
        responses.DELETE,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = categories.delete(category_id)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.default
