import pytest
import requests
import responses
from requests.auth import AuthBase

from snipeit_wrapper import SnipeIT

MOCK_AUTH_STRING = "This is a MockAuth"
EXPECTED_AUTH = {"Authorization": MOCK_AUTH_STRING}
EXAMPLE_SNIPEIT_URL = "https://example.snipeit.io"


class ExpectedResponse:
    default = {"json": "data"}
    payload_send = {"payload": {}}
    payload_return = {}
    messages_send = {"messages": "success"}
    messages_return = "success"
    list_send = {"rows": [{}]}
    list_return = [{}]
    rows_send = {"rows": [{}]}
    rows_return = {}


class SnipeITTest(SnipeIT):
    def __init__(self, base_url: str, auth: str):
        self.url = base_url + "/api/v1"
        self.session = requests.Session()
        self.auth = auth


class MockAuth(AuthBase):
    def __call__(self, r):
        r.headers["Authorization"] = MOCK_AUTH_STRING
        return r


@pytest.fixture
def snipeit():
    return SnipeITTest(EXAMPLE_SNIPEIT_URL, MockAuth())


def snipeit_url(endpoint):
    """
    Adds an endpoint to the base SnipeIT URL

    :param endpoint:
        API endpoint section of the URL

    :returns: Full endpoint url
    """
    return EXAMPLE_SNIPEIT_URL + "/api/v1" + endpoint
