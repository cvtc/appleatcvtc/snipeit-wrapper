import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints import Users


@responses.activate
def test_get_by_search(snipeit):
    users = Users(snipeit)
    search_params = {
        "search": "laptop",
        "order": "asc",
        "sort": "name",
    }
    expected_url = snipeit_url("/users")
    expected_params = {
        "search": "laptop",
        "order": "asc",
        "sort": "name",
        "limit": "100",
        "offset": "0",
    }
    responses.add(responses.GET, expected_url, json=ExpectedResponse.list_send)

    response = users.get_by_search(**search_params)
    assert len(responses.calls) == 1
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    users = Users(snipeit)
    expected_url = snipeit_url("/users/1")
    responses.add(responses.GET, expected_url, json=ExpectedResponse.default)

    response = users.get_by_id(1)
    assert len(responses.calls) == 1
    assert response == ExpectedResponse.default


@responses.activate
def test_create(snipeit):
    users = Users(snipeit)
    expected_url = snipeit_url("/users")
    responses.add(responses.POST, expected_url, json=ExpectedResponse.payload_send)

    response = users.create(ExpectedResponse.default)
    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    users = Users(snipeit)
    expected_url = snipeit_url("/users/1")
    responses.add(responses.PATCH, expected_url, json=ExpectedResponse.payload_send)

    response = users.update(1, ExpectedResponse.default)
    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    users = Users(snipeit)
    expected_url = snipeit_url("/users/1")
    responses.add(responses.PUT, expected_url, json=ExpectedResponse.payload_send)

    response = users.replace(1, ExpectedResponse.default)
    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    users = Users(snipeit)
    expected_url = snipeit_url("/users/1")
    responses.add(responses.DELETE, expected_url, json=ExpectedResponse.messages_send)

    response = users.delete(1)
    assert len(responses.calls) == 1
    assert response == ExpectedResponse.messages_return
