import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints import Manufacturers


@responses.activate
def test_get_by_search(snipeit):
    manufacturers = Manufacturers(snipeit)
    search_params = {
        "name": "Apple",
    }
    expected_url = snipeit_url("/manufacturers")
    expected_params = {
        "name": "Apple",
        "limit": "100",
        "offset": "0",
    }

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
    )
    response = manufacturers.get_by_search(**search_params)
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    manufacturers = Manufacturers(snipeit)
    expected_url = snipeit_url("/manufacturers/1")
    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
    )
    response = manufacturers.get_by_id(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_create(snipeit):
    manufacturers = Manufacturers(snipeit)
    expected_url = snipeit_url("/manufacturers")
    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.payload_send,
    )
    response = manufacturers.create({})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    manufacturers = Manufacturers(snipeit)
    expected_url = snipeit_url("/manufacturers/1")
    responses.add(
        responses.PATCH,
        expected_url,
        json=ExpectedResponse.payload_send,
    )
    response = manufacturers.update(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    manufacturers = Manufacturers(snipeit)
    expected_url = snipeit_url("/manufacturers/1")
    responses.add(
        responses.PUT,
        expected_url,
        json=ExpectedResponse.payload_send,
    )
    response = manufacturers.replace(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    manufacturers = Manufacturers(snipeit)
    expected_url = snipeit_url("/manufacturers/1")
    responses.add(
        responses.DELETE,
        expected_url,
        json=ExpectedResponse.messages_send,
    )
    response = manufacturers.delete(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return
