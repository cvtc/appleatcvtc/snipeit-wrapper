import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints import Locations


@responses.activate
def test_get_by_search(snipeit):
    locations = Locations(snipeit)
    search_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
    }
    expected_url = snipeit_url("/locations")
    expected_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "limit": "100",
        "offset": "0",
    }
    responses.add(
        responses.GET, expected_url, json=ExpectedResponse.list_send, status=200
    )
    response = locations.get_by_search(**search_params)
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    locations = Locations(snipeit)
    expected_url = snipeit_url("/locations/1")
    responses.add(
        responses.GET, expected_url, json=ExpectedResponse.default, status=200
    )
    response = locations.get_by_id(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_create(snipeit):
    locations = Locations(snipeit)
    expected_url = snipeit_url("/locations")
    responses.add(
        responses.POST, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = locations.create({})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    locations = Locations(snipeit)
    expected_url = snipeit_url("/locations/1")
    responses.add(
        responses.PATCH, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = locations.update(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    locations = Locations(snipeit)
    expected_url = snipeit_url("/locations/1")
    responses.add(
        responses.PUT, expected_url, json=ExpectedResponse.payload_send, status=200
    )
    response = locations.replace(1, {})
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    locations = Locations(snipeit)
    expected_url = snipeit_url("/locations/1")
    responses.add(
        responses.DELETE, expected_url, json=ExpectedResponse.messages_send, status=200
    )
    response = locations.delete(1)
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return
