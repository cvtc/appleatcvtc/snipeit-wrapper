import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints.assets import Assets


@responses.activate
def test_get_by_search(snipeit):
    assets = Assets(snipeit)
    search_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "model_id": 1,
        "category_id": 2,
        "manufacturer_id": 3,
        "company_id": 4,
        "location_id": 5,
        "status": "RTD",
        "status_id": 6,
    }
    expected_url = snipeit_url("/hardware")
    expected_params = {
        "search": "laptop",
        "sort": "name",
        "order": "asc",
        "model_id": "1",
        "category_id": "2",
        "manufacturer_id": "3",
        "company_id": "4",
        "location_id": "5",
        "status": "RTD",
        "status_id": "6",
        "limit": "100",
        "offset": "0",
    }

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
        status=200,
    )

    response = assets.get_by_search(**search_params)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.params == expected_params
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    assets = Assets(snipeit)
    asset_id = 123
    expected_url = snipeit_url(f"/hardware/{asset_id}")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = assets.get_by_id(asset_id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_get_by_asset_tag(snipeit):
    assets = Assets(snipeit)
    asset_tag = "ABC123"
    expected_url = snipeit_url(f"/hardware/bytag/{asset_tag}")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = assets.get_by_asset_tag(asset_tag, deleted=False)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.default


@responses.activate
def test_get_by_serial(snipeit):
    assets = Assets(snipeit)
    serial = "123456"
    expected_url = snipeit_url(f"/hardware/byserial/{serial}?deleted=False")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.rows_send,
        status=200,
    )

    response = assets.get_by_serial(serial, deleted=False)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.rows_return


@responses.activate
def test_create(snipeit):
    assets = Assets(snipeit)
    data = {"name": "Laptop", "model_id": 1, "status_id": 2}
    expected_url = snipeit_url("/hardware")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=201,
    )

    response = assets.create(data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    assets = Assets(snipeit)
    asset_id = 123
    data = {"name": "Laptop"}
    expected_url = snipeit_url(f"/hardware/{asset_id}")

    responses.add(
        responses.PATCH,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = assets.update(asset_id, data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    assets = Assets(snipeit)
    asset_id = 123
    data = {"name": "Laptop"}
    expected_url = snipeit_url(f"/hardware/{asset_id}")

    responses.add(
        responses.PUT,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = assets.replace(asset_id, data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return == {}


@responses.activate
def test_delete(snipeit):
    assets = Assets(snipeit)
    asset_id = 123
    expected_url = snipeit_url(f"/hardware/{asset_id}")

    responses.add(
        responses.DELETE,
        expected_url,
        status=200,
        json=ExpectedResponse.messages_send,
    )

    response = assets.delete(asset_id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return
