import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints.fieldsets import Fieldsets


@responses.activate
def test_get_all(snipeit):
    fieldsets = Fieldsets(snipeit)
    expected_url = snipeit_url("/fieldsets")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
        status=200,
    )

    response = fieldsets.get_all()

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    fieldsets = Fieldsets(snipeit)
    id = 123
    expected_url = snipeit_url(f"/fieldsets/{id}")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = fieldsets.get_by_id(id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_get_fields_by_id(snipeit):
    fieldsets = Fieldsets(snipeit)
    id = 123
    expected_url = snipeit_url(f"/fieldsets/{id}/fields")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
        status=200,
    )

    response = fieldsets.get_fields_by_id(id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.list_return


@responses.activate
def test_create(snipeit):
    fieldsets = Fieldsets(snipeit)
    data = {"name": "Test Fieldset"}
    expected_url = snipeit_url("/fieldsets")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fieldsets.create(data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    fieldsets = Fieldsets(snipeit)
    id = 123
    data = {"name": "Test Fieldset"}
    expected_url = snipeit_url(f"/fieldsets/{id}")

    responses.add(
        responses.PATCH,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fieldsets.update(id, data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    fieldsets = Fieldsets(snipeit)
    id = 123
    data = {"name": "Test Fieldset"}
    expected_url = snipeit_url(f"/fieldsets/{id}")

    responses.add(
        responses.PUT,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fieldsets.replace(id, data)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.payload_return


# TODO: Fix delete test, it's giving a chunk error for some reason
"""
@responses.activate
def test_delete(snipeit):
    fieldsets = Fieldsets(snipeit)
    id = 123
    expected_url = snipeit_url(f"/fieldsets/{id}")

    responses.add(
        responses.DELETE,
        expected_url,
        json=ExpectedResponse.messages_send,
        status=204,
    )

    response = fieldsets.delete(id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return
    """
