import responses
from base_test import ExpectedResponse, snipeit, snipeit_url

from snipeit_wrapper.endpoints.fields import Fields


@responses.activate
def test_get_all(snipeit):
    fields = Fields(snipeit)
    expected_url = snipeit_url("/fields")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.list_send,
        status=200,
    )

    response = fields.get_all()

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.list_return


@responses.activate
def test_get_by_id(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    expected_url = snipeit_url(f"/fields/{field_id}")

    responses.add(
        responses.GET,
        expected_url,
        json=ExpectedResponse.default,
        status=200,
    )

    response = fields.get_by_id(field_id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.default


@responses.activate
def test_associate_with_fieldset(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    fieldset_id = 456
    expected_url = snipeit_url(f"/fields/{field_id}/associate")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.messages_send,
        status=200,
    )

    response = fields.associate_with_fieldset(field_id, fieldset_id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return


@responses.activate
def test_dissociate_with_fieldset(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    fieldset_id = 456
    expected_url = snipeit_url(f"/fields/{field_id}/dissociate")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.messages_send,
        status=200,
    )

    response = fields.dissociate_with_fieldset(field_id, fieldset_id)

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == expected_url
    assert response == ExpectedResponse.messages_return


@responses.activate
def test_create(snipeit):
    fields = Fields(snipeit)
    expected_url = snipeit_url("/fields")

    responses.add(
        responses.POST,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fields.create(ExpectedResponse.default)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_update(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    expected_url = snipeit_url(f"/fields/{field_id}")

    responses.add(
        responses.PATCH,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fields.update(field_id, ExpectedResponse.default)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_replace(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    expected_url = snipeit_url(f"/fields/{field_id}")

    responses.add(
        responses.PUT,
        expected_url,
        json=ExpectedResponse.payload_send,
        status=200,
    )

    response = fields.replace(field_id, ExpectedResponse.default)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.payload_return


@responses.activate
def test_delete(snipeit):
    fields = Fields(snipeit)
    field_id = 123
    expected_url = snipeit_url(f"/fields/{field_id}")

    responses.add(
        responses.DELETE,
        expected_url,
        json=ExpectedResponse.messages_send,
        status=200,
    )

    response = fields.delete(field_id)

    assert len(responses.calls) == 1
    assert response == ExpectedResponse.messages_return
